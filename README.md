Hey there,

please take a look at this blog article to understand the purpose of this scratch repository:
http://milianw.de/blog/akademy-2014-come-to-my-profiling-101-workshop

To download some test data, run the download_data.sh script in data/. Then build the wordcount
application via the usual steps:

mkdir build
cd build
cmake ..
make

Then you can run it on the test files you downloaded in the data/ directory with:

./wordcount ../data/*.txt

You'll see it's quite slow, esp. when compared to the `wc` utility. Why? Profile the
code, get some hard numbers from the benchmark, then improve the code and try again.
Did the performance improve? If so, rinse, repeat until you are happy with the results.

Some tips that might help you get started:

- don't forget to enable compiler optimizations (e.g. CMAKE_BUILD_TYPE=RelWithDebInfo)
- use `perf stat -r` to run the benchmark.
- try `perf record` and `perf report` to find hotspots, enable call-graphs via
  the `--call-graph dwarf` switch
- try valgrind's callgrind on the wordcount benchmark and look at the data in KCacheGrind
- try valgrind's massif and look at the data in massif-visualizer
- once you optimized the code as much as you could, try to parallelize it! let it run
  on multiple threads for the different input files.
