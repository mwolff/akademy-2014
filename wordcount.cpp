/*
 * Copyright 2014 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <QCoreApplication>
#include <QStringList>
#include <QDebug>

#include "wordcount.h"

int main(int argc, char** argv)
{
  QCoreApplication app(argc, argv);

  QTextStream out(stdout);
  QTextStream err(stderr);

  auto args = app.arguments();
  args.pop_front();

  Result overall;

  foreach (const auto& filePath, args) {
    QFile* file = new QFile(filePath);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
      err << "Failed to open input file: " << filePath << '\n';
      continue;
    }

    out << "calculating word count from file: " << filePath << "..." << endl;
    const auto result = wordCount(file);
    delete file;
    out << result << endl;

    if (args.size() > 1) {
      overall.words += result.words;
      overall.lines += result.lines;
      if (result.longestWord.size() > overall.longestWord.size()) {
        overall.longestWord = result.longestWord;
      }
      foreach (const QString& word, result.wordHistogram.keys()) {
        overall.wordHistogram[word] += result.wordHistogram[word];
      }
    }
  }

  if (args.size() > 1) {
    out << "Overall results for all files combined:" << endl << overall << endl;
  }

  return 0;
}
