/*
 * Copyright 2014 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef WORDCOUNT_H
#define WORDCOUNT_H

#include <QTextStream>
#include <QFile>

struct Result
{
  quint64 words = 0;
  quint64 lines = 0;
  QString longestWord;
  QMap<QString, quint64> wordHistogram;
};

QTextStream& operator<<(QTextStream& out, const Result& result)
{
  QString mostFrequentWord;
  quint64 occurrences = 0;
  foreach (const QString& word, result.wordHistogram.keys()) {
    out << result.wordHistogram[word] << '\t' << word << endl;
    if (result.wordHistogram[word] > occurrences) {
      mostFrequentWord = word;
      occurrences = result.wordHistogram[word];
    }
  }
  out << endl;

  out << "words: " << result.words << endl
      << "lines: " << result.lines << endl
      << "longest word: \"" << result.longestWord << "\" (" << result.longestWord.size() << " chars)" << endl
      << "most frequent word: \"" << mostFrequentWord << "\" (" << occurrences << " times)" << endl
      << "distinct words: " << result.wordHistogram.size() << endl;

  return out;
}

Result wordCount(QFile* file)
{
  Result result;

  const QStringList lines = QString::fromUtf8(file->readAll()).split('\n');

  result.lines = lines.size();

  for (const auto &line : lines ) {
    QRegExp pattern("\\b(\\w+)\\b");
    result.words += line.count(pattern);
    int offset = 0;
    while (true) {
      int match = pattern.indexIn(line, offset);
      if (match == -1) {
        break;
      }
      offset = match + pattern.matchedLength();

      const auto word = pattern.cap(1);

      // find longest word
      if (word.length() > result.longestWord.length()) {
        result.longestWord = word;
      }

      // build histogram with only unique, to-lower variants of the words
      const auto lower = word.toLower();
      if (result.wordHistogram.contains(lower)) {
        result.wordHistogram[lower]++;
      } else {
        result.wordHistogram.insert(lower, 1);
      }
    }
  }

  return result;
};

#endif // WORDCOUNT_H
